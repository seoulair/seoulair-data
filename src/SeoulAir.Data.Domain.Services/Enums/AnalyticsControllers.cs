using System.ComponentModel;

namespace SeoulAir.Data.Domain.Services.Enums
{
    public enum AnalyticsControllers : byte
    {
        [Description("api/DataRecord")]
        DataRecord,
    }
}
