﻿namespace SeoulAir.Data.Domain.Dtos
{
    public class BaseDtoWithId
    {
        public string Id { get; set; }
    }
}
