﻿using SeoulAir.Data.Domain.Dtos;

namespace SeoulAir.Data.Domain.Interfaces.Services
{
    public interface IAirPollutionService : ICrudBaseService<DataRecordDto>
    {
    }
}
