﻿using SeoulAir.Data.Domain.Dtos;

namespace SeoulAir.Data.Domain.Interfaces.Repositories
{
    public interface IAirPollutionRepository : ICrudBaseRepository<DataRecordDto>
    {
    }
}
