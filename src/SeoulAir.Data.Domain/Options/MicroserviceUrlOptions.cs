namespace SeoulAir.Data.Domain.Options
{
    public abstract class MicroserviceUrlOptions
    {
        public string Address { get; set; }
        public int Port { get; set; }
    }
}
